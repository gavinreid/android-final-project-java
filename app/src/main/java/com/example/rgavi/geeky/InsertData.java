package com.example.rgavi.geeky;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rgavi.geeky.database.DBHelper;

public class InsertData extends AppCompatActivity {

    String fp ="";

    private static final int PICKFILE_RESULT_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert_data);

    }

    public void pushDb(View view) {
        EditText ff = (EditText)findViewById(R.id.editText2);
       // EditText e = (EditText)findViewById(R.id.editText2);
       String s = ff.getText().toString();
        if(s != ""){   // fp != "" &&

            //String ff = "get this somehow";
            Context mContext = getApplicationContext();
            DBHelper helper = new DBHelper(mContext);
            SQLiteDatabase db = helper.getReadableDatabase();

            long tot = DatabaseUtils.queryNumEntries(db, "items");
            String j = "" + tot;
            ContentValues insertValues = new ContentValues();
            insertValues.put("itemID", j);
            insertValues.put("itemName", s);
            insertValues.put("Content", "user addition");
            insertValues.put("image", fp);
            db.insert("items", null, insertValues);
            Toast.makeText(getApplicationContext(),"Added", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(getApplicationContext(),"Please fill all the fields", Toast.LENGTH_SHORT).show();
        }

    }

    public void chooseImg(View view) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
      //
        //intent.putExtra(android.content.Intent.EXTRA_SUBJECT, title);
        //intent.putExtra(android.content.Intent.EXTRA_TEXT, R.string.visita);
       // Uri uri = Uri
            //    .parse("android.resource://com.package.xname/drawable/"
                //        + i);
       // intent.putExtra(Intent.EXTRA_STREAM, uri);
        startActivity(Intent.createChooser(intent, "Select music"));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
       EditText f = findViewById(R.id.editText3);
        switch (requestCode) {
            case PICKFILE_RESULT_CODE:
                if (resultCode == RESULT_OK) {
                    fp = data.getData().getPath();
                    f.setText(fp);
                }
                break;

        }
    }
}
