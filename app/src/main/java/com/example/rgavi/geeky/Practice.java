package com.example.rgavi.geeky;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.example.rgavi.geeky.database.DBHelper;
import com.example.rgavi.geeky.model.DataItem;
import com.example.rgavi.geeky.database.DBHelper;

import java.util.List;
import java.util.Random;
import java.util.Vector;

public class Practice extends AppCompatActivity {


    List<String> name = new Vector<String>();
    List<String> image = new Vector<String>();
    List<String> id = new Vector<String>();
    String ggg;
    Random rand = new Random();
    private Context mContext;
    int tt;

    int status = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_practice);
        this.mContext = getApplicationContext();
        DBHelper helper = new DBHelper(mContext);
        SQLiteDatabase db = helper.getReadableDatabase();
        long tot = DatabaseUtils.queryNumEntries(db, "items");
        tt = (int)tot;
        Cursor cursor = db.rawQuery("select * from items", null);
        if(cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                String id0 = cursor.getString(cursor.getColumnIndex("itemID"));
                String name0 = cursor.getString(cursor.getColumnIndex("itemName"));
                String image0 = cursor.getString(cursor.getColumnIndex("image"));
                id.add(id0);
                name.add(name0);
                image.add(image0);
                cursor.moveToNext();
            }
        }
    }

    long totalSeconds = 30;
    long intervalSeconds = 1;

    CountDownTimer timer = new CountDownTimer(totalSeconds * 1000, intervalSeconds * 1000) {

        public void onTick(long millisUntilFinished) {

           // Log.d("seconds elapsed: " , (totalSeconds * 1000 - millisUntilFinished) / 1000);
            TextView t = findViewById(R.id.textView6);
           // t.setTextColor(Color.parseColor("#FF0000"));

            t.setText("seconds elapsed: " + (totalSeconds * 1000 - millisUntilFinished) / 1000);

            if(((totalSeconds * 1000 - millisUntilFinished) / 1000) > 5){


            }
        }

        public void onFinish() {
            //Log.d( "done!", "Time's up!");
        }

    };


    public void startTime(View view) {
        TextView t = findViewById(R.id.textView4);
        EditText et = findViewById(R.id.editText);
        ggg = name.get(rand.nextInt(tt));
        // create a function to pull the topic from the database assing to two below
        et.setText(" ");
        et.setHint("Write a function that performs a " + ggg +". No cheating ;)");
        t.setText(ggg);
        //t.setHeight(500);
        t.setTextSize(TypedValue.COMPLEX_UNIT_SP, 30f);
        timer.start();
    }

    private void hideSoftKeyboard(View view){
        InputMethodManager imm=
                (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(),0);
    }

    public void done(View view) { // fix this up
        hideSoftKeyboard(view);
        Button b = findViewById(R.id.button2);
        if (status == 0){
            startTime(view);


            ++status;
            b.setText("Done");
        }else if(status == 1){
            EditText et = findViewById(R.id.editText);
           String gj = et.getText().toString();
            et.setHint(gj);
            timer.cancel();
            b.setBackgroundResource(0);
            b.setText("View Solution?");
            ++status;
        }else if(status == 2){
            b.setText(" ");

            Resources res = getResources();

            int resID = res.getIdentifier(ggg, "drawable", getPackageName());

            b.setBackgroundResource(resID);
            EditText e = findViewById(R.id.editText);
            ++status;
        }else if(status == 3){
            b.setBackgroundResource(0);
            b.setText("Next?");
            TextView t = findViewById(R.id.textView4);
            t.setText("Practice");
            ++status;
            // handle on clicks rest status from here
        }else if(status == 4){

            // set up to replay the function
            startTime(view);


            b.setBackgroundColor(Color.parseColor("#000000"));

            b.setText("Done");
            status = 1;
        }

        /*if(b.getText() == "View Solution?"){
            b.setText(" ");
            b.setBackgroundResource(R.drawable.qq2);
            EditText e = findViewById(R.id.editText);
           // e.clearFocus();
            //InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
           // imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

        }else{
            timer.cancel();
            b.setBackgroundResource(0);
            b.setText("View Solution?");
        }
        */



    }

    public void Next(View view) {

            // setup to load the new data in
        /*
        Button b = findViewById(R.id.button2);
        b.setBackgroundColor(Color.parseColor("#000000"));

            status = 0;
        done(view);
        */
    }
};

