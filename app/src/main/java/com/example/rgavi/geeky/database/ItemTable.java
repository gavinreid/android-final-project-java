package com.example.rgavi.geeky.database;

/**
 * Created by rgavi on 12/6/2017.
 */

public class ItemTable {
    public static final String TABLE_ITEMS = "items";
    public static final String COLUMN_ID = "itemID";
    public static final String COLUMN_NAME = "itemName";
    public static final String COLUMN_Content = "Content";
    public static final String COLUMN_IMAGE = "image";

    public static final String SQL_CREATE=
            "CREATE TABLE " + TABLE_ITEMS + "(" +
                    COLUMN_ID + " TEXT," +
                    COLUMN_NAME + " TEXT," +
                    COLUMN_IMAGE + " TEXT," +
                    COLUMN_Content + " TEXT" +
                    ");";
    public static final String SQL_DELETE=
            "DROP TABLE " + TABLE_ITEMS;

}
