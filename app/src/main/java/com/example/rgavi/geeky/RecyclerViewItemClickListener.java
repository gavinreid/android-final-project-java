package com.example.rgavi.geeky;

import android.view.View;

/**
 * Created by rgavi on 11/30/2017.
 */
abstract public interface RecyclerViewItemClickListener {
    public void onClick(View view, int position);

    public void onLongClick(View view, int position);


}

