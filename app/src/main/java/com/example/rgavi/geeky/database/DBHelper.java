package com.example.rgavi.geeky.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by rgavi on 12/6/2017.
 */

public class DBHelper extends SQLiteOpenHelper {

    public static final String DB_File_Name = "algoBox.db";
    public static final int DB_VERSON = 12;



    public DBHelper(Context context) {

        super(context, DB_File_Name, null, DB_VERSON);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(ItemTable.SQL_CREATE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(ItemTable.SQL_DELETE);
        onCreate(db);
    }
}
