package com.example.rgavi.geeky;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.DrawableRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import static java.lang.Math.toIntExact;

import com.example.rgavi.geeky.database.DBHelper;
import com.example.rgavi.geeky.model.DataItem;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Vector;


public class WhatAlgo extends AppCompatActivity {

    boolean init = true;
    List<String> name = new Vector<String>();
    List<String> image = new Vector<String>();
    List<String> id = new Vector<String>();
    int tt = 0;
    Boolean enlarged = false;

private String response ="linear_search";
private int score = 0;
private int streak = 0;
private int badStreak = 0;
    private List<DataItem> mItems;
    public int HIYA = 0;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_what_algo);
        this.mContext = getApplicationContext();
        DBHelper helper = new DBHelper(mContext);
        SQLiteDatabase db = helper.getReadableDatabase();
       long tot = DatabaseUtils.queryNumEntries(db, "items");
        tt = (int)tot;
        Cursor cursor = db.rawQuery("select * from items", null);
        if(cursor.moveToFirst()){
            while(!cursor.isAfterLast()){
                String id0 = cursor.getString(cursor.getColumnIndex("itemID"));
                String name0 = cursor.getString(cursor.getColumnIndex("itemName"));
                String image0 = cursor.getString(cursor.getColumnIndex("image"));
                id.add(id0);
                name.add(name0);
                image.add(image0);
                cursor.moveToNext();
            }
        }
    }


    public void loadData(){
        ImageView mmm = findViewById(R.id.imageView3);

        int ii = image.size();
        Random randomGenerator = new Random();

        RadioButton x = findViewById(R.id.radioButton);
        RadioButton y = findViewById(R.id.radioButton2);
        RadioButton z = findViewById(R.id.rb3);
        Random rand = new Random();

        int j = rand.nextInt(tt);
        int k = j;
        while(k == j) {
            k = rand.nextInt(tt);
        }
        int l = k;
        while(l == k || l == j ) {
            l = rand.nextInt(tt);
        }
        int two = rand.nextInt(2);

        if(two == 0){
            two = k;
            response = name.get(k);
        }else if(two == 1){
            two = j;
            response = name.get(j);
        }else if(two == 2){
            two = l;
            response = name.get(l);
        }
        Resources res = getResources();
        String mDrawableName = image.get(two);
        int resID = res.getIdentifier(mDrawableName, "drawable", getPackageName());
        mmm.setImageResource(resID);

        x.setText(name.get(l));
        y.setText(name.get(j));
        z.setText(name.get(k));
        rand = new Random();

    }

    public void streaker(){
        streak = 0;
        badStreak++;
        if (badStreak == 5) {
            Toast.makeText(getApplicationContext(), "Take it easy bro", Toast.LENGTH_SHORT).show();
            if (badStreak == 25) {
                Toast.makeText(getApplicationContext(), "Geez time to go back to bed", Toast.LENGTH_SHORT).show();
            }
            if (badStreak == 50) {
                Toast.makeText(getApplicationContext(), "Soooo i'm impressed", Toast.LENGTH_SHORT).show();
            }
            if (badStreak == 100) {
                Toast.makeText(getApplicationContext(), " I count that as an accomplishment", Toast.LENGTH_SHORT).show();
            }
            if (badStreak == 500) {
                Toast.makeText(getApplicationContext(), " Way to stick with it", Toast.LENGTH_SHORT).show();
            }

            if (badStreak == 1000) {
                Toast.makeText(getApplicationContext(), " Well I quit", Toast.LENGTH_SHORT).show();
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.exit(0);
            }
        }
    }

    public void submitBtn(View view) {

        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.radioGroup);

        int radioButtonID = radioGroup.getCheckedRadioButtonId();

        RadioButton radioButton = (RadioButton)

                radioGroup.findViewById(radioButtonID);

        String selectedtext = (String) radioButton.getText();

         // inputs selection options and image

        Toast.makeText(getApplicationContext(), "Selected: " + selectedtext + " \nCorrect Answer: " + response + " \nScore: " + score + " " + streak(), Toast.LENGTH_SHORT).show();

        if (selectedtext.equals(response)) {

            score += 1;

            HIYA++;

            TextView i = findViewById(R.id.textView5);
            i.setText("Points: " + score);
            streak += 0;
            badStreak = 0;

        } else {

            streaker(); // evaluates the current streak

        }
        loadData();

        ImageView i = findViewById(R.id.imageView3);

        enlarged = false;

        if (init) {
            init = false;
        }
        i.getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;
       // response = "binary_search";

    }


    String streak(){
        TextView i = findViewById(R.id.textView5);
        if(streak >= 5){
            i.setText("Points: " + score + "  Streak: " + streak);
            return  "Well Done Streak of " + streak;
        }else if(badStreak >= 5){
            i.setText("Points: " + score + "  Bad Streak: " + badStreak);
            return  "Take it easy, Bad Streak " + badStreak;
        }
        return " ";
    }

    public void enlarge(View view) {

        ImageView i = findViewById(R.id.imageView3);
        if(enlarged == false) {

            DisplayMetrics displayMetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            int height = displayMetrics.heightPixels;
            int width = displayMetrics.widthPixels;
            i.getLayoutParams().height = 500;
            i.getLayoutParams().width = width;
            i.requestLayout();
            enlarged  = true;
        }else{
            i.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
            i.getLayoutParams().width= ViewGroup.LayoutParams.MATCH_PARENT;
            i.requestLayout();
            enlarged = false;
        }
    }
}
