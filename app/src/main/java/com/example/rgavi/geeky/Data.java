package com.example.rgavi.geeky;

/**
 * Created by rgavi on 11/29/2017.
 */

public class Data {
    public String title;
    public String description;
    public int imageId;


    Data(String title, String description, int imageId) {
        this.title = title;
        this.description = description;
        this.imageId = imageId;
    }
}
