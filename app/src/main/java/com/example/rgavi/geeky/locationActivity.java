package com.example.rgavi.geeky;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

public class locationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);
        Toast.makeText(getApplicationContext(), "Try these sites for practice", Toast.LENGTH_LONG).show();
    }
            // prompt for open in browser or app
    public void Open(View view) {
        String url = "https://www.codechef.com/problems/school";
        //if (!url.startsWith("http://") && !url.startsWith("https://")) url = "http://" + url;
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    public void b2(View view) {

        String url = "https://practice.geeksforgeeks.org/topic-tags#DataStructures";
        //if (!url.startsWith("http://") && !url.startsWith("https://")) url = "http://" + url;
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    public void bt3(View view) {

        String url = "https://leetcode.com/problemset/algorithms/";
        //if (!url.startsWith("http://") && !url.startsWith("https://")) url = "http://" + url;
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    public void bt4(View view) {
        String url = "https://www.hackerearth.com/practice/";
        //if (!url.startsWith("http://") && !url.startsWith("https://")) url = "http://" + url;
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    public void bt5(View view) {
        String url = "https://projecteuler.net/archives";
        //if (!url.startsWith("http://") && !url.startsWith("https://")) url = "http://" + url;
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    public void bt6(View view) {

        ArrayList<String> updatableList = new ArrayList<String>();



        updatableList.add("jain");
       String r1 =  "https://coderbyte.com/challenges?a=true";
       String r2 = "https://www.hackerrank.com/dashboard";
       String r3 = "https://www.codingame.com/training";
       String r4 = "https://www.hacksplaining.com/exercises";
       String r5 = "https://www.topcoder.com/challenges/?bucket=past";
        updatableList.add(r1);
        updatableList.add(r2);
        updatableList.add(r3);
        updatableList.add(r4);
        updatableList.add(r5);
        int randomNum = ThreadLocalRandom.current().nextInt(0, 3 + 1);
        //Toast.makeText(getApplicationContext(), randomNum, Toast.LENGTH_SHORT).show();
        String url = updatableList.get(randomNum);
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }
}
