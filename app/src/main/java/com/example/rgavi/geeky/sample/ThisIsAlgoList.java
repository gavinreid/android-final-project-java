package com.example.rgavi.geeky.sample;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.example.rgavi.geeky.model.DataItem;


/**
 * Created by rgavi on 12/6/2017.
 */

public class ThisIsAlgoList {
    public static List<DataItem> dataItemList;
    public static Map<String, DataItem> dataItemMap;

    static {
        dataItemList = new ArrayList<>();
        dataItemMap = new HashMap<>();

        addItem(new DataItem("1", "linear_search", "content is something", "linear_search"));
        addItem(new DataItem("2", "bucket_sort", "content is something", "bucket_sort"));
        addItem(new DataItem("3", "comb_sort", "content is something", "comb_sort"));
        addItem(new DataItem("4", "count_sort", "content is something", "count_sort"));
        addItem(new DataItem("5", "exponental_search", "content is something", "exponental_search"));
        addItem(new DataItem("6", "fibonacci", "content is something", "fibonacci"));
        addItem(new DataItem("7", "heap_sort", "content is something", "heap_sort"));
        addItem(new DataItem("8", "shell_sort", "content is something", "shell_sort"));
        addItem(new DataItem("9", "selection_sort", "content is something", "selection_sort"));
        addItem(new DataItem("10", "pigeonhole_sort", "content is something", "pigeonhole_sort"));
        addItem(new DataItem("11", "interpolated_search", "content is something", "interpolated_search"));
        addItem(new DataItem("12", "ternary_search", "content is something", "ternary_search"));
        addItem(new DataItem("13", "jump_search", "content is something", "jump_search"));
        addItem(new DataItem("14", "quick_sort", "content is something", "quick_sort"));
        addItem(new DataItem("15", "radix_sort", "content is something", "radix_sort"));
        addItem(new DataItem("16", "dijkstra_min_path", "content is something", "dijkstra_min_path"));



        // addItem(new DataItem("2", "binarySearch", "content is something", "search3442.PNG"));
       // addItem(new DataItem("3", "jumpSearch", "make this the url??", "JumpSearch.PNG"));
       // addItem(new DataItem("4", "combSort", "content is something", "combSort.PNG"));
       // addItem(new DataItem("5", "cycleSort", "content is something", "cycleSort.PNG"));
       // addItem(new DataItem("6", "countSort", "content is something", "countSort.PNG"));
       // addItem(new DataItem("7", "exponentalSearch", "content is something", "exponentalSearch.PNG"));

    }
    private static void addItem(DataItem item) {
        dataItemList.add(item);
        dataItemMap.put(item.getItemID(), item);
    }
    }
