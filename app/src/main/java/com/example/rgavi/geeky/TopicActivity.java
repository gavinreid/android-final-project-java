package com.example.rgavi.geeky;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class TopicActivity extends AppCompatActivity {



    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topic);

        List<Data> data = fill_with_data();

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        Recycler_View_Adapter adapter = new Recycler_View_Adapter(data, getApplication());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        recyclerView.addOnItemTouchListener(new CustomRVItemTouchListener(this, recyclerView, new RecyclerViewItemClickListener() {
            @Override
            public void onClick(View view, int position) {
                Toast.makeText(getApplicationContext(), "clicked", Toast.LENGTH_SHORT).show();


                switch (position){
                    case 0:
                        //practice
                        Intent intent2 = new Intent(TopicActivity.this, Practice.class);
                        startActivity(intent2);
                        //Toast.makeText(getApplicationContext(), "You can't go there", Toast.LENGTH_SHORT).show();
                        break;
                    case 1:
                        //What algo quiz
                        Intent intent = new Intent(TopicActivity.this, WhatAlgo.class);
                        startActivity(intent);
                        break;
                    case 2:
                        Intent in = new Intent(TopicActivity.this, InsertData.class);
                        startActivity(in);

                        break;
                    case 3:
                        Toast.makeText(getApplicationContext(), "clicked ", Toast.LENGTH_SHORT).show();
                        Intent intent5 = new Intent(TopicActivity.this, locationActivity.class);
                        startActivity(intent5);
                        break;
                    case 4:
                        break;

                    case 5:
                        break;
                    case 6:
                        break;

                    case 7:

                        break;
                    case 8:
                        /*
                        if (getIntent().hasExtra("bundle") && (savedInstanceState == null))
                            savedInstanceState = getIntent().getExtras().getBundle("bundle");

                        // Call setTheme before creation of any(!) View.
                        setTheme(android.R.style.Theme_Black);


                        super.onCreate(savedInstanceState);
                        setContentView(R.layout.activity_topic);
                        */
                       // Toast.makeText(getApplicationContext(), " here ", Toast.LENGTH_SHORT).show();
                      //  View v = findViewById(R.id.layD);
                      //  v.setBackgroundColor(0xFFEE3333);

/*
                        setTheme(R.style.CardView_Dark);

                        setContentView(R.layout.activity_topic);
                        onCreate();
                        */
                      //  RelativeLayout lLayout = (RelativeLayout) findViewById(R.id.layD);
                       // lLayout.setBackgroundColor(Color.parseColor("#000000"));
                        //RelativeLayout lLayout = (RelativeLayout) findViewById(R.id.layD);
                        //lLayout.setBackgroundColor(Color.parseColor("#000000"));
                       // recreate();

                        break;
                    case 9:







                        break;


                }


            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }
    String randQuestion(){


        return "Under Construction";
    }

    public List<Data> fill_with_data() {

        List<Data> data = new ArrayList<>();
        data.add(new Data("Practice", System.lineSeparator()+"Practice writing out algorithms from memory"
                , R.drawable.chess));
        data.add(new Data("Master Identifier", System.lineSeparator()+"Identify the Algorithm  ", R.drawable.bio ));

        data.add(new Data("Add to Archive", System.lineSeparator()+"Add additional topics to the Database", R.drawable.ic_add_black_24dp));

        data.add(new Data("Practice Locations", System.lineSeparator()+"Sites for practice", R.drawable.prloc));


        return data;
    }

}
