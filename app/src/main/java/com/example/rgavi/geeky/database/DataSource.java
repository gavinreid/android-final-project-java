package com.example.rgavi.geeky.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.rgavi.geeky.model.DataItem;

/**
 * Created by rgavi on 12/6/2017.
 */

public class DataSource {
    private Context mContext;
    private SQLiteDatabase mdatabase;
    private SQLiteOpenHelper mdbHelper;

    public DataSource(Context context) {
        this.mContext = context;
        mdbHelper = new DBHelper(mContext);
        mdatabase = mdbHelper.getWritableDatabase();
    }
        public void open(){
            mdatabase = mdbHelper.getWritableDatabase();
    }

        public  void close(){
            mdbHelper.close();
    }

    public DataItem createItem(DataItem item){
        ContentValues values = item.toValues();
        mdatabase.insert(ItemTable.TABLE_ITEMS, null, values);
        return item;
    }

    public long getDataItemsCount(){
        return DatabaseUtils.queryNumEntries(mdatabase, ItemTable.TABLE_ITEMS);
    }
}
