package com.example.rgavi.geeky.model;

import android.content.ContentValues;
import android.os.Parcel;
import android.os.Parcelable;

import com.example.rgavi.geeky.database.ItemTable;

import java.util.UUID;

/**
 * Created by rgavi on 12/6/2017.
 */

public class DataItem implements Parcelable {


    private String itemID;
    private String itemName;
    private String Content;
    private String image;

    public DataItem(String itemId, String itemName, String Content, String image) {
        if (itemId == null) {
            itemId = UUID.randomUUID().toString();
        }
        this.itemID = itemId;
        this.itemName = itemName;
        this.Content = Content;
        this.image = image;
    }

    public String getItemID(){
        return itemID;
    }
    public String getitemName(){
        return itemName;
    }
    public String getContent(){
        return Content;
    }
    public String getimage(){
        return image;
    }
    public void setitemID(String id){
        this.itemID = id;
    }
    public void setitemName(String name){
        this.itemName = name;
    }
    public void setContent(String c){
        this.Content = c;
    }
    public void setimage(String i){
        this.image = i;
    }

    public ContentValues toValues(){
        ContentValues values = new ContentValues(7);

        values.put(ItemTable.COLUMN_ID, itemID);
        values.put(ItemTable.COLUMN_NAME, itemName);
        values.put(ItemTable.COLUMN_Content, Content);
        values.put(ItemTable.COLUMN_IMAGE, image);

        return values;
    }



    public static final Creator<DataItem> CREATOR = new Creator<DataItem>() {
        @Override
        public DataItem createFromParcel(Parcel in) {
            return new DataItem(in);
        }

        @Override
        public DataItem[] newArray(int size) {
            return new DataItem[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.itemID);
        dest.writeString(this.itemName);
        dest.writeString(this.Content);
        dest.writeString(this.image);

    }

    public DataItem(Parcel in) {
        this.itemID = in.readString();
        this.itemName = in.readString();
        this.Content = in.readString();
        this.image = in.readString();

    }

    @Override
    public String toString() {
        return "DataItem{" +
                "itemId='" + itemID + '\'' +
                ", itemName='" + itemName + '\'' +
                ", content='" + Content + '\'' +
                ", image='" + image + '\'' +
                '}';
    }


}
